<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\PostRequest;
use App\Models\Post;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{

    public function index(): Collection
    {
        return Post::all();
    }

    public function store(PostRequest $request): JsonResponse
    {
        $user = Auth::user();
        $user->posts()->create($request->validated());
        return response()->json([
            'status' => 'success',
            'message' => 'Post created!'
        ], 201);
    }

    public function show(Post $post): Post
    {
        return $post;
    }

    public function update(PostRequest $request, Post $post): JsonResponse
    {
        $post->update($request->validated());
        return response()->json([
            'status' => 'success',
            'message' => 'Post updated!'
        ]);
    }

    public function destroy(Post $post): JsonResponse
    {
        $post->delete();
        return response()->json([
            'status' => 'success',
            'message' => 'Post deleted!'
        ]);
    }

}
