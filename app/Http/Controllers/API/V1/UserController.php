<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

    public function getPosts()
    {
        $user = Auth::user();
        return $user->posts;
    }

}
