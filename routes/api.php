<?php

use App\Http\Controllers\API\V1\PostController;
use App\Http\Controllers\API\V1\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('auth:sanctum')->prefix('v1')->group(function () {
    Route::apiResource('posts', PostController::class);
    Route::get('user-posts', [UserController::class, 'getPosts']);
});

require __DIR__ . '/auth.php';
