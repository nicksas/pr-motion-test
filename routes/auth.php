<?php

use App\Http\Controllers\API\V1\Auth\AuthenticatedController;
use App\Http\Controllers\API\V1\Auth\RegisteredUserController;
use Illuminate\Support\Facades\Route;

Route::middleware('guest')->prefix('v1')->group(function () {
    Route::post('register', [RegisteredUserController::class, 'store']);
    Route::post('login', [AuthenticatedController::class, 'store']);
});

Route::middleware('auth:sanctum')->prefix('v1')->group(function () {
    Route::post('logout', [AuthenticatedController::class, 'destroy'])
        ->name('logout');
});
