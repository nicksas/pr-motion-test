<?php

namespace Tests\Feature\API\V1\Post;

use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class PostUpdateTest extends TestCase
{
    use RefreshDatabase;

    public function test_update_post_with_valid_data()
    {
        Sanctum::actingAs(
            User::factory()->create()
        );
        $post = Post::factory()->create();

        $title = 'Title new';
        $text = 'Text new';
        $response = $this->put("/api/v1/posts/{$post->id}", [
            'title' => $title,
            'text' => $text,
        ]);
        $this->assertDatabaseHas('posts', ['title' => $title, 'text' => $text]);
        $response->assertStatus(200);
    }

    public function test_can_not_update_post_with_invalid_title()
    {
        Sanctum::actingAs(
            User::factory()->create()
        );
        $post = Post::factory()->create();

        $text = 'Text new';
        $response = $this->put("/api/v1/posts/{$post->id}", [
            'text' => $text,
        ]);
        $response->assertStatus(422);
    }

    public function test_can_not_update_post_with_invalid_text()
    {
        Sanctum::actingAs(
            User::factory()->create()
        );
        $post = Post::factory()->create();

        $title = 'Title new';
        $response = $this->put("/api/v1/posts/{$post->id}", [
            'title' => $title,
        ]);
        $response->assertStatus(422);
    }
}
