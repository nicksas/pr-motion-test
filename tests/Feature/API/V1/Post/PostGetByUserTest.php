<?php

namespace Tests\Feature\API\V1\Post;

use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class PostGetByUserTest extends TestCase
{
    use RefreshDatabase;

    public function test_get_post_by_user()
    {
        $count = 3;

        Sanctum::actingAs(
            User::factory()->has(Post::factory()->count($count))->create(),
        );

        $response = $this->get("/api/v1/user-posts");

        $responseContent = $response->content();
        $responseCountItems = count(json_decode($responseContent));

        $this->assertEquals($count, $responseCountItems);
    }


}
