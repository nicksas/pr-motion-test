<?php

namespace Tests\Feature\API\V1\Post;

use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class PostDeleteTest extends TestCase
{
    use RefreshDatabase;

    public function test_delete_post()
    {
        Sanctum::actingAs(
            User::factory()->create()
        );
        $post = Post::factory()->create();
        $post_id = $post->id;

        $response = $this->delete("/api/v1/posts/{$post_id}");

        $response->assertStatus(200);
    }
}
