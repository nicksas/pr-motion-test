<?php

namespace Tests\Feature\API\V1\Post;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class PostCreateTest extends TestCase
{
    use RefreshDatabase;

    public function test_create_new_post_with_valid_data()
    {
        Sanctum::actingAs(
            User::factory()->create(),
        );

        $title = 'Test post';
        $text = 'text post lorem';
        $response = $this->post('/api/v1/posts', [
            'title' => $title,
            'text' => $text,
        ]);
        $this->assertDatabaseHas('posts', ['title' => $title, 'text' => $text]);
        $response->assertStatus(201);
    }

    public function test_can_not_create_new_post_with_invalid_title()
    {
        Sanctum::actingAs(
            User::factory()->create(),
        );

        $text = 'text post lorem';
        $response = $this->post('/api/v1/posts', [
            'text' => $text,
        ]);
        $response->assertStatus(422);
    }

    public function test_can_not_create_new_post_with_invalid_text()
    {
        Sanctum::actingAs(
            User::factory()->create(),
        );

        $title = 'Test post';
        $response = $this->post('/api/v1/posts', [
            'title' => $title,
        ]);
        $response->assertStatus(422);
    }
}
