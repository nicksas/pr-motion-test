<?php

namespace Tests\Feature\API\V1\Post;

use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class PostIndexTest extends TestCase
{
    use RefreshDatabase;

    public function test_get_all_posts()
    {
        $count = 5;

        Sanctum::actingAs(
            User::factory()->has(Post::factory()->count($count))->create(),
        );

        $response = $this->get("/api/v1/posts");

        $responseContent = $response->content();
        $responseCountItems = count(json_decode($responseContent));

        $this->assertEquals($count, $responseCountItems);
    }


}
